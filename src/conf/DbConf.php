<?php

namespace conf;

use Illuminate\Database\Capsule\Manager as DB;

class DbConf
{
    public static function init() {
        $db = new DB();
        $db->addConnection(parse_ini_file('db.ini'));
        $db->setAsGlobal();
        $db->bootEloquent();
    }
}