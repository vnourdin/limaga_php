<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 16/01/2016
 * Time: 10:41
 */

namespace limaga\model;


class Ebillet extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'ebillet';
    protected $primaryKey = 'idBillet';
    public $timestamps = false;

    public function relationPanier() {
        return $this->hasManyThrough('\limaga\model\Panier', 'contientbillet', 'id_panier', 'id_billet');
    }
}