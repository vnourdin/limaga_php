<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 20/01/2016
 * Time: 08:13
 */

namespace limaga\model;


class MembreFamille extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'membrefamille';
    protected $primaryKey = 'idMembre';
    public $timestamps = false;

    public function relationClient()
    {
        return $this->hasManyThrough('\limaga\model\MembreFamille', 'famille', 'idClient', 'idMembre');
    }

}