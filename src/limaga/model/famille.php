<?php
class Famille extends \Illuminate\Database\Eloquent\Model {
	protected $table = 'famille';
	protected $primaryKey = 'idClient';
	
	public $timestamps = false;
}