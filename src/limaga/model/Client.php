<?php

namespace limaga\model;


class Client extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'client';
    protected $primaryKey = 'idClient';
    public $timestamps = false;

    public function relationPanier()
    {
        return $this->hasMany('\limaga\model\Panier', 'id_client');
    }

    public function relationFacture()
    {
        return $this->hasMany('\limaga\model\Facture', 'id_client');
    }

    public function relationMembre()
    {
        return $this->hasManyThrough('\limaga\model\MembreFamille', 'famille', 'idClient', 'idMembre');
    }
}
