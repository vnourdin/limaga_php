<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 17/01/2016
 * Time: 11:41
 */

namespace limaga\model;


class Abonnement  extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'abonnement';
    protected $primaryKey = 'idAbonnement';
    public $timestamps = false;

    public function relationPanier() {
        return $this->hasManyThrough('\limaga\model\Panier', 'contientabonnement', 'id_panier', 'id_abonnement');
    }

}