<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 05/01/2016
 * Time: 20:58
 */

namespace limaga\model;


class Produit extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'produit';
    protected $primaryKey = 'idProduit';
    public $timestamps = false;

    public function relationPanier() {
        return $this->hasManyThrough('\limaga\model\Panier', 'contientproduit', 'id_panier', 'id_produit');
    }

}
