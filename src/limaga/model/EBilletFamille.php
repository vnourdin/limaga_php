<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 20/01/2016
 * Time: 08:06
 */

namespace limaga\model;


class EBilletFamille extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'EBilletFamille';
    protected $primaryKey = 'idBilletFamille';
    public $timestamps = false;

    public function relationPanier() {
        return $this->hasManyThrough('\limaga\model\Panier', 'contientbilletfamille', 'id_panier', 'id_billetFamille');
    }

    public function relationBilletFamille() {
        return $this->belongsToMany('\limaga\model\EbilletFamille', "contientbilletfamille", 'id_panier', 'id_billetFamille');
    }
}