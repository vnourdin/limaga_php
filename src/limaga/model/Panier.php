<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 10/01/2016
 * Time: 12:17
 */

namespace limaga\model;


class Panier extends \Illuminate\Database\Eloquent\Model
{

    protected $table = 'panier';
    protected $primaryKey = 'idPanier';
    public $timestamps = false;

    public function relationClient() {
        return $this->belongsTo('\limaga\model\Client', "id_client");
    }

    public function relationProduit() {
        return $this->belongsToMany('\limaga\model\Produit', "contientproduit", 'id_panier', 'id_produit')->withPivot('quantite');
    }

    public function relationBillet() {
        return $this->belongsToMany('\limaga\model\Ebillet', "contientbillet", 'id_panier', 'id_billet');
    }

    public function relationAbonnement() {
        return $this->belongsToMany('\limaga\model\Abonnement', "contientabonnement", 'id_panier', 'id_abonnement');
    }

    public function relationBilletFamille() {
        return $this->belongsToMany('\limaga\model\EbilletFamille', "contientbilletfamille", 'id_panier', 'id_billetFamille');
    }

}