<?php

namespace limaga\control;

use limaga\model\Abonnement;
use limaga\model\Client;
use limaga\model\Ebillet;
use limaga\model\EBilletFamille;
use limaga\model\Facture;
use limaga\model\MembreFamille;
use limaga\model\Panier;
use limaga\model\Produit;
use limaga\vue\VueUser;

class UserController extends AbstractController {
	public function __construct() {
		parent::__construct ();
	}
	public function index() {
		$v = new VueUser ();
		echo $v->render ( 1 );
	}
	public function catalogue() {
		$val = Produit::all ();
		$v = new VueUser ( $val );
		echo $v->render ( 2 );
	}
	public function ajoutPanier() {
		if (isset ( $_POST ["ajoutProduit"] ) && isset ( $_POST ['quantite'] )) {
			filter_var ( $_POST ['quantite'], FILTER_SANITIZE_NUMBER_INT );
			
			$panier = new Panier ();
			
			$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
			$idcli = $idc->idClient;
			
			$nbPanier = $idc->whereHas ( 'relationPanier', function ($panier) {
				$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
				$panier->where ( "valide", 0 )->where('id_Client', $idc->idClient);
			} )->get ();
			
			$idProd = Produit::find ( $_POST ['ajoutProduit'] );
			
			// si il n'y a pas de panier en cours
			if (sizeof ( $nbPanier ) == 0) {
				$panier->prixTotal = $idProd->prix * $_POST ['quantite'];
				$panier->id_client = $idcli;
				$panier->save ();
			} else {
				$id = 0;
				while ( $idc->relationPanier [$id]->valide != 0 )
					$id ++;
				$panier = $idc->relationPanier [$id];
				$panier->prixTotal += $idProd->prix * $_POST ['quantite'];
				$panier->save ();
			}
			
			$produits = $panier->relationProduit;
			$prod = null;
			foreach ( $produits as $produit ) {
				if ($produit->idProduit == $idProd->idProduit) {
					$prod = $produit;
					break;
				}
			}
			
			if ($prod == null) {
				$panier->relationProduit ()->attach ( $idProd, array (
						"quantite" => $_POST ['quantite'] 
				) );
			} else {
				$prod->pivot->quantite += $_POST ['quantite'];
				$prod->pivot->save ();
			}
			
			$app = \Slim\Slim::getInstance ();
			$app->redirect ( $app->urlFor ( "catalogue" ) );
		}
	}
	public function ajoutPanierBillet() {
		if (isset ( $_POST ['ajoutBillet'] ) && $_POST ['ajoutBillet'] == 'ajBillet') {
			filter_var ( $_POST ['quantite'], FILTER_SANITIZE_NUMBER_INT );
			
			$panier = new Panier ();
			
			$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
			$idcli = $idc->idClient;
			
			$nbPanier = $idc->whereHas ( 'relationPanier', function ($panier) {
				$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
				$panier->where ( "valide", 0 )->where('id_Client', $idc->idClient);
			} )->get ();
			
			for($i = 0; $i < $_POST ['quantite']; $i ++) {
				$newBillet = new Ebillet ();
				
				// verification que le code barre du billet n'existe pas
				$verif = false;
				$code = rand ( 10000000, 999999999 );
				while ( $verif == false ) {
					$codeExistant = Ebillet::where ( 'codeBarre', '=', $code )->get ();
					if (sizeof ( $codeExistant ) == 0) {
						$verif = true;
					} else {
						$code = rand ( 10000000, 99999999 );
					}
				}
				
				$newBillet->codeBarre = $code;
				$newBillet->save ();
				
				if ($_POST ['quantite'] < 5) {
					$prixUnitaire = 4;
				} elseif ($_POST ['quantite'] < 10) {
					$prixUnitaire = 3;
				} else {
					$prixUnitaire = 2;
				}
				
				if (sizeof ( $nbPanier ) == 0) {
					$panier->prixTotal = $prixUnitaire * $_POST ['quantite'];
					$panier->id_client = $idcli;
					$panier->save ();
				} else {
					$id = 0;
					while ( $idc->relationPanier [$id]->valide != 0 )
						$id ++;
					$panier = $idc->relationPanier [$id];
					$panier->prixTotal += $prixUnitaire * $_POST ['quantite'];
					$panier->save ();
				}
				
				$panier->relationBillet ()->attach ( $newBillet->idBillet );
			}
			$app = \Slim\Slim::getInstance ();
			$app->redirect ( $app->urlFor ( "billet" ) );
		}
	}
	public function ajoutPanierAbonnement() {
		if (isset ( $_POST ['ajoutAbonnement'] ) && $_POST ['ajoutAbonnement'] == 'ajAbonnement') {
			if (isset ( $_POST ['abonnementChoix'] )) {
				filter_var ( $_POST ['quantite'], FILTER_SANITIZE_NUMBER_INT );
				
				$panier = new Panier ();
				$abonnement = new Abonnement ();
				
				$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
				$idcli = $idc->idClient;
				
				$nbPanier = $idc->whereHas ( 'relationPanier', function ($panier) {
					$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
					$panier->where ( "valide", 0 )->where('id_Client', $idc->idClient);
				} )->get ();
				
				$verif = false;
				$code = rand ( 10000000, 999999999 );
				while ( $verif == false ) {
					$codeExistant = Abonnement::where ( 'codeBarre', '=', $code )->get ();
					if (sizeof ( $codeExistant ) == 0) {
						$verif = true;
					} else {
						$code = rand ( 10000000, 99999999 );
					}
				}
				
				if ($_POST ['abonnementChoix'] == 'aboAnnuel') {
					$abonnement->libelle = 'annuel';
					$abonnement->prix = 110;
				} elseif ($_POST ['abonnementChoix'] == 'aboMensuel') {
					$abonnement->libelle = 'mensuel';
					$abonnement->prix = 12;
				}
				$abonnement->codeBarre = $code;
				$abonnement->save ();
				
				if (sizeof ( $nbPanier ) == 0) {
					$panier->prixTotal = $abonnement->prix * $_POST ['quantite'];
					$panier->id_client = $idcli;
					$panier->save ();
				} else {
					$id = 0;
					while ( $idc->relationPanier [$id]->valide != 0 )
						$id ++;
					$panier = $idc->relationPanier [$id];
					$panier->prixTotal += $abonnement->prix * $_POST ['quantite'];
					$panier->save ();
				}
				
				$panier->relationAbonnement ()->attach ( $abonnement->idAbonnement );
				
				$app = \Slim\Slim::getInstance ();
				$app->redirect ( $app->urlFor ( "billet" ) );
			}
		}
	}
	public function billet() {
		$v = new VueUser ();
		echo $v->render ( 3 );
	}
	public function panier() {
		$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
		
		$nbPanier = $idc->whereHas ( 'relationPanier', function ($panier) {
			$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
			$panier->where ( "valide", 0 )->where('id_Client', $idc->idClient);
		} )->get ();
		
		if (sizeof ( $nbPanier ) == 0) {
			$v = new VueUser ();
			echo $v->render ( 5 );
		} else {
			$v = new VueUser ( $idc->relationPanier );
			echo $v->render ( 4 );
		}
	}
	public function profil() {
		$idc = Client::where ( "email", $_SESSION ["email"] )->get ();
		$v = new VueUser ( $idc );
		echo $v->render ( 6 );
	}
	public function modifProfilForm() {
		$v = new VueUser ();
		echo $v->render ( 7 );
	}
	public function redirection() {
		$v = new VueUser ();
		echo $v->render ( 7 );
	}
	public function modifProfil() {
		if (isset ( $_POST ['submitModification'] ) && ($_POST ['submitModification'] == 'subModifProfil')) {
			
			$idc = Client::where ( "email", $_SESSION ["email"] )->get ();
			$erreurs = array ();
			
			if ($_POST ['nom'] != filter_var ( $_POST ['nom'], FILTER_SANITIZE_STRING )) {
				array_push ( $erreurs, "Nom invalide, merci de corriger" );
			}
			if ($_POST ['prenom'] != filter_var ( $_POST ['prenom'], FILTER_SANITIZE_STRING )) {
				array_push ( $erreurs, "Prenom invalide, merci de corriger" );
			}
			if ($_POST ['ville'] != filter_var ( $_POST ['ville'], FILTER_SANITIZE_STRING )) {
				array_push ( $erreurs, "Ville invalide, merci de corriger" );
			}
			if ($_POST ['adresse'] != filter_var ( $_POST ['adresse'], FILTER_SANITIZE_STRING )) {
				array_push ( $erreurs, "Adresse invalide, merci de corriger" );
			}
			if ($_POST ['codePostal'] != filter_var ( $_POST ['codePostal'], FILTER_SANITIZE_NUMBER_INT )) {
				array_push ( $erreurs, "Code postal invalide, merci de corriger" );
			}
			if ($_POST ['tel'] != filter_var ( $_POST ['tel'], FILTER_SANITIZE_NUMBER_INT )) {
				array_push ( $erreurs, "Telephone invalide, merci de corriger" );
			}
			/*
			 * if ($_POST['email'] != filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
			 * array_push($erreurs, "Adresse email invalide, merci de corriger");
			 * } else {
			 * $emailVerif = Client::where('email', $_POST['email'])->get();
			 * if (sizeof($emailVerif) != 0) {
			 * array_push($erreurs, "Un compte à déjà été créé avec cette adresse email");
			 * }
			 * }
			 */
			if ($_POST ['niveauNatation'] == 'Niveau de natation') {
				array_push ( $erreurs, "Niveau de natation invalide, merci de corriger" );
			}
			
			if ($_POST ['mdp'] != filter_var ( $_POST ['mdp'], FILTER_SANITIZE_STRING )) {
				array_push ( $erreurs, "Mot de passe invalide, merci de corriger" );
			}
			if ($_POST ['mdp'] != $_POST ['confpass']) {
				array_push ( $erreurs, "Mot de passe et confirmation différents, merci de corriger" );
			}
			
			if (sizeof ( $erreurs ) == 0) {
				$nom = filter_var ( $_POST ['nom'], FILTER_SANITIZE_STRING );
				$prenom = filter_var ( $_POST ['prenom'], FILTER_SANITIZE_STRING );
				$ville = filter_var ( $_POST ['ville'], FILTER_SANITIZE_STRING );
				$adresse = filter_var ( $_POST ['adresse'], FILTER_SANITIZE_STRING );
				$codePostal = filter_var ( $_POST ['codePostal'], FILTER_SANITIZE_NUMBER_INT );
				$telephone = filter_var ( $_POST ['tel'], FILTER_SANITIZE_NUMBER_INT );
				$pass = password_hash ( $_POST ['mdp'], PASSWORD_DEFAULT, array (
						'cost' => 12 
				) );
				$niveauNatation = null;
				if ($_POST ['niveauNatation'] == "Niveau 1 (débutant)") {
					$niveauNatation = 1;
				} elseif ($_POST ['niveauNatation'] == "Niveau 2 (intermédiaire)") {
					$niveauNatation = 2;
				} elseif ($_POST ['niveauNatation'] == "Niveau 3 (aguerri)") {
					$niveauNatation = 3;
				}
				
				if (($_POST ['nom']) != null) {
					$idc [0]->nom = $nom;
				}
				if (($_POST ['prenom']) != null) {
					$idc [0]->prenom = $prenom;
				}
				if (($_POST ['ville']) != null) {
					$idc [0]->ville = $ville;
				}
				if (($_POST ['adresse']) != null) {
					$idc [0]->adresse = $adresse;
				}
				if (($_POST ['codePostal']) != null) {
					$idc [0]->codePostal = $codePostal;
				}
				if (($_POST ['tel']) != null) {
					$idc [0]->telephone = $telephone;
				}
				/*
				 * if(isset($_POST['email'])) {
				 * $idc[0]->email = $email;
				 * }
				 */
				if (($_POST ['mdp']) != null) {
					$idc [0]->motDePasse = $pass;
				}
				if (($_POST ['niveauNatation']) != null) {
					$idc [0]->niveauNatation = $niveauNatation;
				}
				
				$idc [0]->save ();
				
				$app = \Slim\Slim::getInstance ();
				$app->redirect ( $app->urlFor ( 'profil' ) );
			} else {
				foreach ( $erreurs as $e ) {
					echo $e . "<br />";
				}
			}
		}
	}
	public function deconnexion() {
		if (isset ( $_SESSION ['email'] )) {
			$app = \Slim\Slim::getInstance ();
			session_unset ();
			session_destroy ();
			// on redirige vers l'accueil de invite
			$app->redirect ( $app->urlFor ( 'accueil' ) );
		}
	}
	public function contact() {
		$v = new VueUser ();
		echo $v->render ( 9 );
	}
	public function plan() {
		$v = new VueUser ();
		echo $v->render ( 10 );
	}
	public function horaire() {
		$v = new VueUser ();
		echo $v->render ( 11 );
	}
	public function prestation() {
		$v = new VueUser ();
		echo $v->render ( 13 );
	}
	public function actualite() {
		$v = new VueUser ();
		echo $v->render ( 12 );
	}
	public function creerFacture() {
		$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
		
		$app = \Slim\Slim::getInstance ();
		
		$facture = new \limaga\model\Facture ();
		if (sizeof ( $idc->relationPanier [0]->relationBillet ) == 0 && sizeof ( $idc->relationPanier [0]->relationProduit ) == 0 && sizeof ( $idc->relationPanier [0]->relationAbonnement ) == 0) {
			$app->redirect ( $app->urlFor ( 'panier' ) );
		} else {
			$id = 0;
			while ( $idc->relationPanier [$id]->valide != 0 )
				$id ++;
				
				// calcul prix
			$prix = 0;
			foreach ( $idc->relationPanier [$id]->relationProduit as $p ) {
				$prix += $p->prix * $p->pivot ["quantite"];
			}
			
			$prix += sizeof ( $idc->relationPanier [$id]->relationBillet ) * 4;
			foreach ( $idc->relationPanier [$id]->relationAbonnement as $a ) {
				$prix += $a->prix;
			}
			
			$regle = 0;
			$idcli = $idc->idClient;
			$panier = Panier::where ( 'id_client', $idcli )->where ( 'valide', 0 )->get ();
			
			if ($_POST ["paiement"] == "immediat") {
				$regle = 1;
				$panier [0]->valide = 1;
				$panier [0]->save ();
			}
			
			// moyen de paiement ??
			$facture->id_client = $idc->idClient;
			$facture->montant = $prix;
			$facture->reglee = $regle;
			$facture->dateCreation = date ( "Y-m-d" );
			
			if ($regle == 1)
				$facture->dateReglement = date ( "Y-m-d" );
			
			$facture->id_client = $idc->idClient;
			$facture->id_panier = $idc->relationPanier [0]->idPanier;
			$facture->save ();
		}
		$app->redirect ( $app->urlFor ( 'accueil' ) );
	}
	public function formMembreFamille() {
		$v = new VueUser ();
		echo $v->render ( 8 );
	}
	public function ajoutMembreFamille() {
		if (isset ( $_POST ['ajoutMembreFamille'] ) && $_POST ['ajoutMembreFamille'] == 'ajMembreFamille') {
			if (isset ( $_POST ['nom'] ) && isset ( $_POST ['prenom'] ) && isset ( $_POST ['age'] )) {
				filter_var ( $_POST ['nom'], FILTER_SANITIZE_STRING );
				filter_var ( $_POST ['prenom'], FILTER_SANITIZE_STRING );
				filter_var ( $_POST ['age'], FILTER_SANITIZE_NUMBER_INT );
				
				$idc = Client::where ( 'email', $_SESSION ['email'] )->get () [0];
				
				$newMembreFamille = new MembreFamille ();
				
				$newMembreFamille->nom = $_POST ['nom'];
				$newMembreFamille->prenom = $_POST ['prenom'];
				$newMembreFamille->age = $_POST ['age'];
				
				$newMembreFamille->save ();
				echo 'test1';
				$idc->relationMembre->attach ( $newMembreFamille->idMembre );
				echo 'test2';
			}
		}
	}
	public function achatBilletFamille() {
		if (isset ( $_POST ['ajoutBilletFamille'] ) && $_POST ['ajoutBilletFamille'] == 'ajBilletFamille') {
			if (isset ( $_POST ['quantite'] ) && isset ( $_POST ['membreFamille'] )) {
				filter_var ( $_POST ['quantite'], FILTER_SANITIZE_NUMBER_INT );
				
				$panier = new Panier ();
				
				$idc = Client::where ( "email", $_SESSION ["email"] )->get () [0];
				$idcli = $idc->idClient;
				
				$nbPanier = $idc->whereHas ( 'relationPanier', function ($panier) {
					$panier->where ( "valide", 0 );
				} )->get ();
				
				for($i = 0; $i < $_POST ['quantite']; $i ++) {
					$newBilletFamille = new EBilletFamille ();
					
					// verification que le code barre du billet n'existe pas
					$verif = false;
					$code = rand ( 10000000, 999999999 );
					while ( $verif == false ) {
						$codeExistant = EBilletFamille::where ( 'codeBarre', '=', $code )->get ();
						if (sizeof ( $codeExistant ) == 0) {
							$verif = true;
						} else {
							$code = rand ( 10000000, 99999999 );
						}
					}
					
					$newBilletFamille->codeBarre = $code;
					$newBilletFamille->save ();
					
					if ($_POST ['quantite'] < 3) {
						$prix = 4 * $_POST ['quantite'];
					} elseif ($_POST ['quantite'] <= 5) {
						$prix = (4 * $_POST ['quantite']) * 0.80;
					} else {
						$prix = (4 * $_POST ['quantite']) * 0.70;
					}
					
					if (sizeof ( $nbPanier ) == 0) {
						$panier->prixTotal = $prix;
						$panier->id_client = $idcli;
						$panier->save ();
					} else {
						$panier = $idc->relationPanier [0];
						$panier->prixTotal += $prix;
						$panier->save ();
					}
					
					$panier->relationBilletFamille->attach ( $newBilletFamille->idBilletFamille );
				}
			}
		}
	}
	public function facture() {
		$idc = Client::where ( "email", $_SESSION ["email"] )->get ();
		
		$nbFacture = $idc [0]->relationFacture->where ( 'reglee', '=', 0 );
		
		if (sizeof ( $nbFacture ) == 0) {
			$v = new VueUser ();
			echo $v->render ( 15 );
		} else {
			$v = new VueUser ( $nbFacture );
			echo $v->render ( 14 );
		}
	}
	public function factureReglement() {
		if (isset ( $_POST ['paiementFactureDiffere'] )) {
			$facture = Facture::find ( $_POST ['paiementFactureDiffere'] );
			$facture->reglee = 1;
			$facture->dateReglement = date ( "Y-m-d" );
			$facture->save ();
			
			$app = \Slim\Slim::getInstance ();
			$app->redirect ( $app->urlFor ( 'accueil' ) );
		}
	}
}
