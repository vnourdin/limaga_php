<?php

namespace limaga\control;


use limaga\utils\Authentication;
use limaga\vue\VueInvite;

class InviteController extends AbstractController
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $v = new VueInvite();
        echo $v->render(1);
    }

    public function actualite()
    {
        $v = new VueInvite();
        echo $v->render(2);
    }

    public function planAcces()
    {
        $v = new VueInvite();
        echo $v->render(3);
    }

    public function connexionForm($tabErreurs = null)
    {
        $v = new VueInvite();
        echo $v->render(4, $tabErreurs);
    }

    public function horaire()
    {
        $v = new VueInvite();
        echo $v->render(8);
    }

    public function connexionCo()
    {
        if (isset($_POST['submitConnexion']) && $_POST['submitConnexion'] == 'subConnex') {
            if (isset($_POST['email']) && $_POST['mdp']) {
                Authentication::authenticate($_POST['email'], $_POST['mdp']);
            }
        }
    }

    public function inscriptionForm($tabErreurs = null)
    {
        $v = new VueInvite();
        echo $v->render(5, $tabErreurs);
    }

    public function contact()
    {
        $v = new VueInvite();
        echo $v->render(6);
    }

    public function inscriptionAjout()
    {
        if (isset($_POST['submitInscription']) && ($_POST['submitInscription'] == 'subInscri')) {
            if (isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['dateNaiss']) && isset($_POST['ville']) && isset($_POST['adresse']) && isset($_POST['codePostal']) && isset($_POST['tel']) && isset($_POST['email']) && isset($_POST['niveauNatation']) && isset($_POST['mdp']) && isset($_POST['confpass'])) {
                Authentication::createUser($_POST['nom'], $_POST['prenom'], $_POST['dateNaiss'], $_POST['adresse'], $_POST['codePostal'], $_POST['ville'], $_POST['tel'], $_POST['email'], $_POST['niveauNatation'], $_POST['mdp'], $_POST['confpass']);
            }
        }
    }
}