<?php

namespace limaga\utils;

use limaga\control\InviteController;
use limaga\model\Client;
use Slim\Slim;

class Authentication {
	private static $authSessionVar;
	public static function authenticate($email, $pass) {
		$erreurs = array ();
		$control = new InviteController ();
		
		if (! filter_var ( $email, FILTER_VALIDATE_EMAIL )) {
			array_push ( $erreurs, "Adresse email invalide, merci de corriger" );
		}
		
		if ($pass != filter_var ( $pass, FILTER_SANITIZE_STRING )) {
			array_push ( $erreurs, "Mot de passe invalide, merci de corriger" );
		}
		
		if (sizeof ( $erreurs ) == 0) {
			$email = filter_var ( $email, FILTER_SANITIZE_EMAIL );
			// $pass = filter_var($pass, FILTER_SANITIZE_STRING);
			
			$client = Client::where ( 'email', $email )->get ();
			
			if (sizeof ( $client ) == 1) {
				if (password_verify ( $pass, $client ['0'] ['motDePasse'] )) {
					Authentication::loadProfile ( $email );
					$slim = Slim::getInstance ();
					$slim->redirect ( $slim->urlFor ( "accueil" ) );
				} else {
					array_push ( $erreurs, "Mot de passe faux" );
					$control->connexionForm ( $erreurs );
				}
			} else {
				array_push ( $erreurs, "Utilisateur inconnu" );
				$control->connexionForm ( $erreurs );
			}
		} else {
			$control->connexionForm ( $erreurs );
		}
	}
	public static function loadProfile($email) {
		session_destroy ();
		session_start ();
		$_SESSION ['email'] = $email;
	}
	public static function createUser($nom, $prenom, $dateNaiss, $adresse, $codePostal, $ville, $telephone, $email, $niveauNatation, $pass, $confpass) {
		$c = new Client ();
		$control = new InviteController ();
		$erreurs = array ();
		
		if ($nom != filter_var ( $nom, FILTER_SANITIZE_STRING )) {
			array_push ( $erreurs, "Nom invalide, merci de corriger" );
		}
		if ($prenom != filter_var ( $prenom, FILTER_SANITIZE_STRING )) {
			array_push ( $erreurs, "Prenom invalide, merci de corriger" );
		}
		if ($ville != filter_var ( $ville, FILTER_SANITIZE_STRING )) {
			array_push ( $erreurs, "Ville invalide, merci de corriger" );
		}
		if ($adresse != filter_var ( $adresse, FILTER_SANITIZE_STRING )) {
			array_push ( $erreurs, "Adresse invalide, merci de corriger" );
		}
		if (! filter_var ( $codePostal, FILTER_VALIDATE_FLOAT )) {
			array_push ( $erreurs, "Code postal invalide, merci de corriger" );
		}
		if (! filter_var ( $telephone, FILTER_VALIDATE_FLOAT )) {
			array_push ( $erreurs, "Telephone invalide, merci de corriger" );
		}
		if ($email != filter_var ( $email, FILTER_VALIDATE_EMAIL )) {
			array_push ( $erreurs, "Adresse email invalide, merci de corriger" );
		} else {
			$emailVerif = Client::where ( 'email', $email )->get ();
			if (sizeof ( $emailVerif ) != 0) {
				array_push ( $erreurs, "Un compte à déjà été créé avec cette adresse email" );
			}
		}
		if ($niveauNatation == 'Niveau de natation') {
			array_push ( $erreurs, "Niveau de natation invalide, merci de corriger" );
		}
		
		if ($pass != filter_var ( $pass, FILTER_SANITIZE_STRING )) {
			array_push ( $erreurs, "Mot de passe invalide, merci de corriger" );
		}
		if ($pass != $confpass) {
			array_push ( $erreurs, "Mot de passe et confirmation différents, merci de corriger" );
		}
		
		if (sizeof ( $erreurs ) == 0) {
			$nom = filter_var ( $nom, FILTER_SANITIZE_STRING );
			$prenom = filter_var ( $prenom, FILTER_SANITIZE_STRING );
			$ville = filter_var ( $ville, FILTER_SANITIZE_STRING );
			$adresse = filter_var ( $adresse, FILTER_SANITIZE_STRING );
			$codePostal = filter_var ( $codePostal, FILTER_SANITIZE_NUMBER_INT );
			$telephone = filter_var ( $telephone, FILTER_SANITIZE_NUMBER_INT );
			$email = filter_var ( $email, FILTER_SANITIZE_EMAIL );
			$pass = password_hash ( $pass, PASSWORD_DEFAULT, array (
					'cost' => 12 
			) );
			if ($niveauNatation == "Niveau 1 (débutant)") {
				$niveauNatation = 1;
			} elseif ($niveauNatation == "Niveau 2 (intermédiaire)") {
				$niveauNatation = 2;
			} elseif ($niveauNatation == "Niveau 3 (aguerri)") {
				$niveauNatation = 3;
			}
			
			$c->nom = $nom;
			$c->prenom = $prenom;
			$c->dateNaiss = $dateNaiss;
			$c->ville = $ville;
			$c->adresse = $adresse;
			$c->codePostal = $codePostal;
			$c->telephone = $telephone;
			$c->email = $email;
			$c->niveauNatation = $niveauNatation;
			$c->motDePasse = $pass;
			
			$c->save ();
			
			Authentication::loadProfile ( $email );
			$slim = Slim::getInstance ();
			$slim->redirect ( $slim->urlFor ( "accueil" ) );
		} else {
			$control->inscriptionForm ( $erreurs );
		}
	}
}