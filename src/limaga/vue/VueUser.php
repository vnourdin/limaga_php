<?php

namespace limaga\vue;


use limaga\model\Client;
use limaga\model\Produit;
use Slim\Slim;

class VueUser
{

    private $tableau = array();

    public function __construct($tab = null)
    {
        $this->tableau = $tab;
    }

    public function render($selecteur)
    {
        $app = \Slim\Slim::getInstance();

        $content = null;
        $valOnglet = null;

        switch ($selecteur) {
            case 1 :
                $content = $this->affichageIndex();
                $valOnglet = "Accueil";
                break;
            case 2 :
                $content = $this->affichageCatalogue();
                $valOnglet = "Catalogue";
                break;
            case 3 :
                $content = $this->affichageBilletAbonnement();
                $valOnglet = "Abonnement";
                break;
            case 4 :
                $content = $this->affichagePanier();
                $valOnglet = "Panier";
                break;
            case 5 :
                $content = $this->affichagePanierVide();
                $valOnglet = "Panier";
                break;
            case 6 :
                $content = $this->affichageProfil();
                $valOnglet = "Profil";
                break;
            case 7 :
                $content = $this->affichageFormulaireModifProfil();
                $valOnglet = "Profil";
                break;
            case 8 :
                $content = $this->affichageAjoutMembreFamille();
                $valOnglet = "Profil";
                break;
            case 9 :
                $content = $this->affichageContact();
                $valOnglet = "Contact";
                break;
            case 10 :
                $content = $this->affichagePlan();
                $valOnglet = "Plan d'acces";
                break;
            case 11 :
                $content = $this->affichageHoraire();
                $valOnglet = "Horaire";
                break;
            case 12 :
                $content = $this->affichageActu();
                $valOnglet = "Actualite";
                break;
            case 13 :
                $content = $this->affichagePrestation();
                $valOnglet = "Prestation";
                break;
            case 14 :
                $content = $this->affichageFacture();
                $valOnglet = "Facture";
                break;
            case 15 :
                $content = $this->affichageFactureVide();
                $valOnglet = "Facture";
                break;


        }

        $urlAccueil = $app->urlFor('accueil');
        $urlCatalogue = $app->urlFor('catalogue');
        $urlDeconnexion = $app->urlFor('deconnexion');
        $urlBillet = $app->urlFor('billet');
        $urlPanier = $app->urlFor('panier');
        $urlProfil = $app->urlFor('profil');
        $urlContact = $app->urlFor('contact');
        $urlHoraire = $app->urlFor('horaire');
        $urlPlan = $app->urlFor('plan');
        $urlActu = $app->urlFor('actualite');
        $urlPrestation = $app->urlFor('prestation');
        $urlFacture = $app->urlFor('facture');

        
        $page = <<<END
 <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta name="description" content="">
                    <meta name="author" content="">
                    <title>$valOnglet | LIMAGA</title>

                    <!-- core CSS -->
                    <link href="web/css/bootstrap.min.css" rel="stylesheet">
                    <link href="web/css/font-awesome.min.css" rel="stylesheet">
                    <link href="web/css/animate.min.css" rel="stylesheet">
                    <link href="web/css/prettyPhoto.css" rel="stylesheet">
                    <link href="web/css/main.css" rel="stylesheet">
                    <link href="web/css/responsive.css" rel="stylesheet">
                    <!--[if lt IE 9]>
                    <script src="/web/js/html5shiv.js"></script>
                    <script src="/web/js/respond.min.js"></script>
                    <![endif]-->
                    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
                </head><!--/head-->

                <body class="homepage">

<header id="header">
        <div class="top-bar">
            <div class="container">
                 <div class="row2">
                    <div class="col-sm-6 col-xs-8">
                        <div class="social">
                            <ul class="social-share">
                                <li class="connect"><a href="$urlProfil">Profil</a></li>
                                <li class="connect"><a href="$urlPanier">Panier</a></li>
								<li class="connect"><a href="$urlDeconnexion">Déconnexion</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div><!--/.container-->
        </div><!--/.top-bar-->

        <nav class="navbar navbar-inverse" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="$urlAccueil">LIMAGA</a>
                </div>

                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li><a href="$urlAccueil">Accueil</a></li>
                        <li><a href="$urlActu">Actualités</a></li>
						<li><a href="$urlPrestation">Suivi de prestations</a></li>
						<li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Achat <i class="fa fa-angle-down"></i></a>
                            <ul class="dropdown-menu">
                                <li><a href="$urlBillet">Réservation de billets</a></li>
                                <li><a href="$urlCatalogue">Achat de produit dérivé</a></li>
                            </ul>
                        </li>
                        <li><a href="$urlFacture">Factures</a></li>
						<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">Informations <i class="fa fa-angle-down"></i></a>
							<ul class="dropdown-menu">
								<li><a href="$urlHoraire">Horaires</a></li>
                                <li><a href="$urlPlan">Plan d'àcces</a></li>
								<li><a href="$urlContact">Contacts</a></li>


                            </ul>
                        </li>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->

    </header><!--/header-->
    <body>
    <div class="content">
    $content
    </div>
      <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 LIMAGA . All Rights Reserved.
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="web/js/jquery.js"></script>
    <script src="web/js/bootstrap.min.js"></script>
    <script src="web/js/jquery.prettyPhoto.js"></script>
    <script src="web/js/jquery.isotope.min.js"></script>
    <script src="web/js/main.js"></script>
    <script src="web/js/wow.min.js"></script>
                </body>
            </html>
                </body>
            </html>
END;


        return $page;
    }

    public function affichageIndex()
    {
        $chaine = <<<END
         <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active" style="background-image: url(web/images/slider/bg1.jpg)">
                </div><!--/.item-->

                <div class="item" style="background-image: url(web/images/slider/bg2.jpg)">
                </div><!--/.item-->

                <div class="item" style="background-image: url(web/images/slider/bg3.jpg)">
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->
END;
        return $chaine;
    }

    public function affichageCatalogue()
    {
        $chaine = null;
        foreach ($this->tableau as $array) {
            $libelle = $array['libelle'];
            $description = $array['description'];
            $prix = $array['prix'];

            $id = Produit::where('libelle', '=', $libelle)->get()[0]->idProduit;

            $chaine .= <<<END
            <ul>
                <li>Libelle : $libelle</li>
                <li>Description : $description</li>
                <li>Prix : $prix</li>
            </ul>
            <form action='' method='post' name=''>
                 <input type='number' required='' name="quantite" placeholder='Quantité' min='1' autofocus>
                 <button type="submit" value="$id" name="ajoutProduit">Ajouter au panier</button>
            </form>
END;
        }
        return $chaine;
    }

    public function affichageBilletAbonnement()
    {
        $chaine = <<<END
            <legend>
                Achat de billets individuel
            </legend>
            <form action='' method='post' name=''>
                <input type="number" required='' name="quantite" placeholder='Quantité' min=1 autofocus>
                <button type="submit" value='ajBillet' name='ajoutBillet'>Ajouter au panier</button>
            </form>
            <BR>
            <legend>
                Achat d'abonnements individuel
            </legend>
            <form action='' method='post' name=''>
                <input type='radio' name='abonnementChoix' value='aboMensuel'>Abonnement mensuel (12€)
                <input type='radio' name='abonnementChoix' value='aboAnnuel'>Abonnement annuel (110€)
                <input type="number" required='' name="quantite" placeholder='Quantité' min=1>
                <button type="submit" value='ajAbonnement' name='ajoutAbonnement'>Ajouter au panier</button>
            </form>

            <legend>
                Achat de billet famille
            </legend>
END;
        $idc = Client::where("email", $_SESSION["email"])->get()[0];

        $membreFamille = $idc->relationFamille;

        if (sizeof($membreFamille) == 0) {
            $chaine .= <<<END
                Vous n'avez pas encore de membre de votre famille ? Vous pouvez en ajouter à partir de votre profil !
END;
        } else {
            $chaine .= <<<END
                <form action='' method='post' name=''>
END;
            foreach ($membreFamille as $membre) {
                $chaine .= <<<END
                    <input type='checkbox' name='membreFamille' value='$membre->idMembre'> $membre->nom $membre->prenom
END;
            }
            $chaine .= <<<END
                    <input type="number" name="quantite" required='' placeholder='Quantité' min=1 autofocus>
                    <button type="submit" value='ajBilletFamille' name='ajoutBilletFamille'>Ajouter au panier</button>
                </form>
END;
        }
        return $chaine;
    }

    public function affichagePanier()
    {
        $chaine = "";
        $prix = 0;
        $app = Slim::getInstance();
        //produit derive
        $id = 0;
        
        while ( $this->tableau[$id]->valide != 0 )
        	$id ++;
        $produit = $this->tableau[$id]->relationProduit;
        if (sizeof($produit) != 0) {
            $chaine .= "produit derive : ";
            foreach ($produit as $p) {
                $chaine .= "<h5>$p->libelle : </h5><div>$p->description</div>";
                $q = $p->pivot['quantite'];
                $chaine .= "<div>quantite : $q</div><div>Prix : " . $q * $p->prix . "E</div>";
                $prix += $q * $p->prix;
                $chaine .= "<button name=suppression value=$p->libelle type=submit>Retirer produit</button><hr/>";
            }
        }

        //billet
        $billet = $this->tableau[$id]->relationBillet;
        if (sizeof($billet) != 0) {
            $chaine .= "<p>billet : " . sizeof($billet) . " commandes</p><p>Prix : " . 4 * sizeof($billet) . "E";
            $chaine .= "<button name=suppresion value=billet type=submit>Retirer billet</button><hr/>";
        }
        $prix += 4 * sizeof($billet);


        //abonnement
        $abonnement = $this->tableau[$id]->relationAbonnement;
        if (sizeof($abonnement) != 0) {
            foreach ($abonnement as $a) {
                $chaine .= "<div>abonnement $a->libelle :</div><div>Prix : $a->prix E</div>";
                $chaine .= "<button name=suppresion value=$a->libelle type=submit>Retirer abonnement</button><hr/>";
                $prix += $a->prix;
            }
        }
        $chaine .= "<div>Prix total : $prix E</div>";

        //boutons a adapter selon besoin
        $chaine .= "<form action='' method='post' name=''><button name='paiement' value='immediat' type='submit'>Paiement immediat</button></form>";
        $chaine .= "<form action='' method='post' name=''><button name='paiement' value='differe' type='submit'>Paiement differe</button></form>";
        return $chaine;
    }

    public function affichagePanierVide()
    {
        $chaine = "Votre panier est vide.";
        return $chaine;
    }

    public function affichageProfil()
    {
        $chaine = null;
        foreach ($this->tableau as $array) {
            $nom = $array['nom'];
            $prenom = $array['prenom'];
            $date = $array['dateNaiss'];
            $adresse = $array['adresse'];
            $ville = $array['ville'];
            $cp = $array['codePostal'];
            $email = $array['email'];
            $tel = $array['telephone'];

            $lvlNatation = null;
            if ($array['niveauNatation'] == 1) {
                $lvlNatation = 'Niveau 1 (débutant)';
            } elseif ($array['niveauNatation'] == 1) {
                $lvlNatation = 'Niveau 2 (intermédiaire)';
            } elseif ($array['niveauNatation'] == 3) {
                $lvlNatation = 'Niveau 3 (aguerri)';
            }

            $app = \Slim\Slim::getInstance();
            $urlModifProfil = $app->urlFor('modificationProfil');
            $urlAjoutMembreFamille = $app->urlFor('ajoutMembreFamille');

            $chaine .= <<<END
                <ul>
                    <li>Nom : $nom</li>
                    <li>Prenom : $prenom</li>
                    <li>Date naissance : $date</li>
                    <li>Adresse : $adresse</li>
                    <li>Ville : $ville</li>
                    <li>Code postal : $cp</li>
                    <li>Email : $email</li>
                    <li>Numéro téléphone : $tel</li>
                    <li>Niveau natation : $lvlNatation</li>
                </ul>

                <a href="$urlModifProfil">Modifier votre profil</a>

                <a href="$urlAjoutMembreFamille">Ajouter des membres à votre famille</a>
END;
        }

        return $chaine;
    }

    public function affichageFormulaireModifProfil()
    {
        $chaine = <<<END
            <form class="form-horizontal" name="formInscription" action="" method="post">
               <fieldset>

<!-- Form Name -->
<legend>Modification du profil : </legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Nom">Nom</label>
  <div class="col-md-4">
  <input id="Nom" name="nom" placeholder="Nom" class="form-control input-md" type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Prenom">Prénom</label>
  <div class="col-md-4">
  <input id="Prenom" name="prenom" placeholder="Prénom" class="form-control input-md" type="text">
    </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="naiss">Date de naissance</label>
  <div class="col-md-4">
  <input id="naiss" name="dateNaiss" placeholder="Date de naissance" class="form-control input-md"  type="date">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adresse">Adresse</label>
  <div class="col-md-4">
  <input id="adresse" name="adresse" placeholder="Adresse" class="form-control input-md"  type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cp">Code Postal</label>
  <div class="col-md-4">
  <input id="cp" name="codePostal" placeholder="Code Postal" class="form-control input-md" type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ville">Ville</label>
  <div class="col-md-4">
  <input id="ville" name="ville" placeholder="Ville" class="form-control input-md" type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tel">Téléphone</label>
  <div class="col-md-4"> <input id="telephone" name="tel" placeholder="0601020304" class="form-control input-md" type="text">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="niveau">Niveau en Natation</label>
  <div class="col-md-4">
    <select id="niveau" name="niveauNatation" class="form-control">
      <option value="1">Niveau 1 (Débutant)</option>
      <option value="2">Niveau 2 (Intermédiaire)</option>
      <option value="3">Niveau 3 (Aguerri)</option>
    </select>
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="login">Email</label>
  <div class="col-md-4">
  <input id="login" name="email" placeholder="azerty@email.fr" class="form-control input-md"  type="text">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass">Mot de passe</label>
  <div class="col-md-4">
    <input id="pass" name="mdp" placeholder="*******" class="form-control input-md" type="password">

  </div>
</div><!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="confpass">Confirmation Mot de passe</label>
  <div class="col-md-4">
    <input id="confpass" name="confpass" placeholder="*******" class="form-control input-md"  type="password">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submitModification" value='subModifProfil' class="btn btn-default">Modifier</button>
  </div></div>

</fieldset>
            </form>
END;
        return $chaine;
    }

    public function affichageAjoutMembreFamille()
    {
        $chaine = <<<END
            <legend>Ajouter Membre Famille</legend>

            <form class ="form-horizontal"name='' action='' method='post'>
             <fieldset>
                <input class="col-md-4 control-label" type='text' name='nom' required='' placeholder='nom'>
                <input  class="col-md-4 control-label" type='text' name='prenom' required='' placeholder='prenom'>
                <input  class="col-md-4 control-label" type='number' name='age' min='0' required='' placeholder='age'>
                <button  class="col-md-4 control-label" type='submit' name='ajoutMembreFamille' value='ajMembreFamille'>Ajouter cette personne</button>
                 </fieldset>
            </form>
END;
        return $chaine;
    }

    public function affichagePlan()
    {
        $chaine = <<<END
            <p>Plan acces</p>
END;
        return $chaine;
    }

    public function affichageHoraire()
    {
        $chaine = <<<END
            <p>Horaire</p>
END;
        return $chaine;
    }

    public function affichageContact()
    {
        $chaine = <<<END
            <p>Contact</p>
END;
        return $chaine;
    }

    public function affichagePrestation()
    {
        $chaine = <<<END
            <p>Prestation</p>
END;
        return $chaine;
    }

    public function affichageActu()
    {
        $chaine = <<<END
            <legend>Actualites : </legend>
<img src ="web/images/slider/bg1.jpg" alt ="actu">
                <p></img>Nous sommes heureux de vous annoncer que la piscine de l'agglomération de LIMAGA réouvre ses portes après
                   sa rénovation, vous pouvez d'ores et déjà vous inscrire sur notre site et ou réserver vos billets. n'hésitez pas
                   à nous contacter pour donner vos avis.</p>
END;
        return $chaine;
    }

    public function affichageFacture()
    {
        $chaine = "<legend>Factures : </legend>";
        foreach($this->tableau as $array) {
            $id = $array['idFacture'];
            $montant = $array['montant'];
            $dateCreat = $array['dateCreation'];

            $chaine .= <<<END
                <ul>
                    <li>Identifiant de la facture : $id</li>
                    <li>Montant : $montant</li>
                    <li>Date de création : $dateCreat</li>
                </ul>

            <form method='post' action="" name=''>
                <button type="submit" name="paiementFactureDiffere" value=$id>Payer la facture</button>
            </form>
END;
        }
        return $chaine;
    }

    public function affichageFactureVide() {
        $chaine = <<<END
            Vous n'avez pas de facture à régler
END;
        return $chaine;
    }
}
