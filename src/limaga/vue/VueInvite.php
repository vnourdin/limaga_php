<?php

namespace limaga\vue;


class VueInvite
{

    private $tableau;

    public function __construct($array = null)
    {
        $this->tableau = $array;
    }

    /**
     * @param $selecteur
     * @param null $tabErreurs
     * @return string
     */
    public function render($selecteur, $tabErreurs = null)
    {
        $app = \Slim\Slim::getInstance();

        $content = null;
        $valOnglet = null;

        if ($selecteur == 1) {
            $content = $this->affichageIndex();
            $valOnglet = "Accueil";
        } elseif ($selecteur == 2) {
            $content = $this->affichageActualite();
            $valOnglet = "Actualités";
        } elseif ($selecteur == 3) {
            $content = $this->affichagePlanAcces();
            $valOnglet = "Plan accès";
        } elseif ($selecteur == 4) {
            $content = $this->affichageConnexionForm($tabErreurs);
            $valOnglet = "Connexion";
        } elseif ($selecteur == 5) {
            $content = $this->affichageInscriptionForm($tabErreurs);
            $valOnglet = "Inscription";
        } elseif ($selecteur == 6) {
            $content = $this->affichageContact();
            $valOnglet = "Contact";
        } elseif ($selecteur == 7) {
            $content = $this->affichageInscriptionErreur($tabErreurs);
            $valOnglet = "Inscription";
        } elseif ($selecteur == 8) {
            $content = $this->affichageHoraire();
            $valOnglet = "Horaire";
        }

        $urlAccueil = $app->urlFor("accueil");
        $urlActualite = $app->urlFor('actualite');
        $urlPlanAcces = $app->urlFor('planAcces');
        $urlConnexion = $app->urlFor('connexion');
        $urlInscription = $app->urlFor('inscription');
        $urlContact = $app->urlFor('contact');
        $urlHoraire = $app->urlFor('horaire');


        $page = <<<END
           <!DOCTYPE html>
            <html lang="fr">
                <head>
                    <meta charset="utf-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                    <meta name="description" content="">
                    <meta name="author" content="">
                    <title>$valOnglet | LIMAGA</title>

                    <!-- core CSS -->
                    <link href="web/css/bootstrap.min.css" rel="stylesheet">
                    <link href="web/css/font-awesome.min.css" rel="stylesheet">
                    <link href="web/css/animate.min.css" rel="stylesheet">
                    <link href="web/css/prettyPhoto.css" rel="stylesheet">
                    <link href="web/css/main.css" rel="stylesheet">
                    <link href="web/css/responsive.css" rel="stylesheet">
                    <!--[if lt IE 9]>
                    <script src="/web/js/html5shiv.js"></script>
                    <script src="/web/js/respond.min.js"></script>
                    <script src="/web/js/map.js"></script>
		            <script src="/web/js/apiJs.js"></script>
                    <![endif]-->
                    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
                    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
                </head><!--/head-->

                <body class="homepage">

                    <header id="header">
                        <div class="top-bar">
                            <div class="container">
                                 <div class="row2">
                                    <div class="col-sm-6 col-xs-8">
                                      <div class="social">
                                            <ul class="social-share">
                                                <li class="connect"><a href="$urlConnexion">Connexion</a></li>
                                                <li class="connect"><a href="$urlInscription">Inscription</a></li>
                                            </ul>
                                       </div>
                                    </div>
                                </div>
                            </div><!--/.container-->
                        </div><!--/.top-bar-->

                        <nav class="navbar navbar-inverse" role="banner">
                            <div class="container">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="$urlAccueil">LIMAGA</a>
                                </div>

                                <div class="collapse navbar-collapse navbar-right">
                                    <ul class="nav navbar-nav">
                                        <li><a href="$urlAccueil">Accueil</a></li>
                                        <li><a href="$urlActualite">Actualités</a></li>
                                       <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Informations <i class="fa fa-angle-down"></i></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="$urlHoraire">Horaires</a></li>
                                                <li><a href="$urlPlanAcces">Plan d'àcces</a></li>
                                                <li><a href="$urlContact">Contacts</a></li>
                                    </ul>
                                </div>
                            </div><!--/.container-->
                        </nav><!--/nav-->

                    </header><!--/header-->
                    <div class="content">
                        $content
                    </div>
            <footer id="footer" class="midnight-blue">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; 2015 LIMAGA . All Rights Reserved.
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

    <script src="web/js/jquery.js"></script>
    <script src="web/js/bootstrap.min.js"></script>
    <script src="web/js/jquery.prettyPhoto.js"></script>
    <script src="web/js/jquery.isotope.min.js"></script>
    <script src="web/js/main.js"></script>
    <script src="web/js/wow.min.js"></script>
                </body>
            </html>
END;
        return $page;
    }

    /**
     * methode d'affichage de la page d'accueil
     * @return string
     */
    public function affichageIndex()
    {
        $chaine = <<<END
            <section id="main-slider" class="no-margin">
        <div class="carousel slide">
            <ol class="carousel-indicators">
                <li data-target="#main-slider" data-slide-to="0" class="active"></li>
                <li data-target="#main-slider" data-slide-to="1"></li>
                <li data-target="#main-slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="item active" style="background-image: url(web/images/slider/bg1.jpg)">
                </div><!--/.item-->

                <div class="item" style="background-image: url(web/images/slider/bg2.jpg)">
                </div><!--/.item-->

                <div class="item" style="background-image: url(web/images/slider/bg3.jpg)">
                </div><!--/.item-->
            </div><!--/.carousel-inner-->
        </div><!--/.carousel-->
        <a class="prev hidden-xs" href="#main-slider" data-slide="prev">
            <i class="fa fa-chevron-left"></i>
        </a>
        <a class="next hidden-xs" href="#main-slider" data-slide="next">
            <i class="fa fa-chevron-right"></i>
        </a>
    </section><!--/#main-slider-->
END;
        return $chaine;
    }

    /**
     * methode d'affichage de la page d'actualite
     * @return string
     */
    public function affichageActualite()
    {
        $chaine = <<<END
            <legend>Actualites : </legend>
<img src ="web/images/slider/bg1.jpg" alt ="actu">
                <div class = "actu">
                <p></img>Nous sommes heureux de vous annoncer que la piscine de l'agglomération de LIMAGA réouvre ses portes après
                   sa rénovation, vous pouvez d'ores et déjà vous inscrire sur notre site et ou réserver vos billets. n'hésitez pas
                   à nous contacter pour donner vos avis.</p>
                </div>

END;
        return $chaine;
    }

    /**
     * methode d'affichage de la page du plan d'acces
     * @return string
     */
    public function affichagePlanAcces()
    {
        $chaine = <<<END
            <p>Plan acces</p>
END;
        return $chaine;
    }

    /**
     * methode d'affichage du formulaire de connexion
     * @return string
     */
    public function affichageConnexionForm($tabErreurs = null)
    {
        $chaine = "";

        if ($tabErreurs != null) {
            foreach ($tabErreurs as $erreur) {
                $chaine .= "<div>$erreur</div>";
            }
        }

        $chaine .= <<<END
            <form class="form-horizontal" name="formConnexion" action="" method="post">
                <fieldset>
                <legend>Connexion : </legend>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="login">Email</label>
                        <div class="col-md-4">
                            <input id="login" name="email" placeholder="azerty@email.fr" class="form-control input-md" required="" type="text" autofocus>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="pass">Mot de passe</label>
                        <div class="col-md-4">
                            <input id="pass" name="mdp" placeholder="*******" class="form-control input-md" required="" type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for="submit"></label>
                        <div class="col-md-4">
                            <button type="submit" id="submit" name="submitConnexion" value="subConnex" class="btn btn-default">Se Connecter</button>
                      </div>
                    </div>
                </fieldset>
            </form>
END;
        return $chaine;
    }

    /**
     * methode d'affichage du formulaire d'inscription
     * @return string
     */
    public function affichageInscriptionForm($tabErreurs = null)
    {
        $chaine = "";

        if ($tabErreurs != null) {
            foreach ($tabErreurs as $erreur) {
                $chaine .= "<div>$erreur</div>";
            }
        }

        $chaine .= <<<END
            <form class="form-horizontal" name="formInscription" action="" method="post">
               <fieldset>

<!-- Form Name -->
<legend>Inscription : </legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Nom">Nom</label>
  <div class="col-md-4">
  <input id="Nom" name="nom" placeholder="Nom" class="form-control input-md" required="" type="text" autofocus>

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="Prenom">Prénom</label>
  <div class="col-md-4">
  <input id="Prenom" name="prenom" placeholder="Prénom" class="form-control input-md" required="" type="text">
    </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="naiss">Date de naissance</label>
  <div class="col-md-4">
  <input id="naiss" name="dateNaiss" placeholder="Date de naissance" class="form-control input-md" required="" type="date">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="adresse">Adresse</label>
  <div class="col-md-4">
  <input id="adresse" name="adresse" placeholder="Adresse" class="form-control input-md" required="" type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="cp">Code Postal</label>
  <div class="col-md-4">
  <input id="cp" name="codePostal" placeholder="Code Postal" class="form-control input-md" required="" type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="ville">Ville</label>
  <div class="col-md-4">
  <input id="ville" name="ville" placeholder="Ville" class="form-control input-md" required="" type="text">

  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="tel">Téléphone</label>
  <div class="col-md-4"> <input id="telephone" name="tel" placeholder="0601020304" class="form-control input-md" required="" type="text">

  </div>
</div>

<!-- Select Basic -->
<div class="form-group">
  <label class="col-md-4 control-label" for="niveau">Niveau en Natation</label>
  <div class="col-md-4">
    <select id="niveau" name="niveauNatation" class="form-control">
      <option value="1">Niveau 1 (Débutant)</option>
      <option value="2">Niveau 2 (Intermédiaire)</option>
      <option value="3">Niveau 3 (Aguerri)</option>
    </select>
  </div>
</div>
<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="login">Email</label>
  <div class="col-md-4">
  <input id="login" name="email" placeholder="azerty@email.fr" class="form-control input-md" required="" type="text">

  </div>
</div>

<!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="pass">Mot de passe</label>
  <div class="col-md-4">
    <input id="pass" name="mdp" placeholder="*******" class="form-control input-md" required="" type="password">

  </div>
</div><!-- Password input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="confpass">Confirmation Mot de passe</label>
  <div class="col-md-4">
    <input id="confpass" name="confpass" placeholder="*******" class="form-control input-md" required="" type="password">

  </div>
</div>

<!-- Button -->
<div class="form-group">
  <label class="col-md-4 control-label" for="submit"></label>
  <div class="col-md-4">
    <button id="submit" name="submitInscription" value='subInscri' class="btn btn-default">S'inscrire</button>
  </div></div>

</fieldset>
            </form>
END;
        return $chaine;
    }

    /**
     * methode d'affichage de la page contact
     * @return string
     */
    public function affichageContact()
    {
        $chaine = <<<END
            <p >Contact</p>
END;
        return $chaine;
    }

    /**
     * methode d'affichage une fois le formulaire d'inscription remplis
     * @return string
     */
    public function affichageInscriptionErreur($tabErreurs)
    {
        $err = "";
        foreach ($tabErreurs as $erreur) {
            $err .= $erreur . "<br />";
        }
        $chaine = <<<END
            <h1>Inscription</h1>
            <p>$err</p>
END;
        return $chaine;
    }

    public function affichageHoraire()
    {
        $chaine = <<<END
            <p>Piscine d'agglomération de communauté de LIMAGA<br />
            6 bis boulevard Henri Barbusse</br />
            96223 LIMAGA</p>

            <div id="emplacementMap"></div>

            <style>
                #emplacementMap {
                    width : 300px;
                    height : 300px;
                }
            </style>
END;
        return $chaine;
    }
}
