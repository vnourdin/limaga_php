<?php
/**
 * Created by PhpStorm.
 * User: Nico
 * Date: 05/01/2016
 * Time: 11:08
 */

require 'vendor/autoload.php';


\conf\DbConf::init();

$app = new \Slim\Slim();

session_start();

if(!isset($_SESSION['email'])) {
    $app->get('/', function () {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->index();
    })->name('accueil');

    $app->get('/actualite', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->actualite();
    })->name('actualite');


    $app->get('/planacces', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->planAcces();
    })->name('planAcces');


    // Connexion
    $app->get('/connexion', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->connexionForm();
    })->name('connexion');

    $app->post('/connexion', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->connexionCo();
    });


    // Inscription
    $app->post('/inscription', function () {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->inscriptionAjout();
    })->name('inscription');


    $app->get('/inscription', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->inscriptionForm();
    });


    $app->get('/contact', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->contact();
    })->name('contact');

    $app->get('/horaire', function() {
        $contrInvite = new \limaga\control\InviteController();
        $contrInvite->horaire();
    })->name('horaire');
}
elseif (isset($_SESSION['email'])) {
    $app->get('/', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->index();
    })->name('accueil');

    $app->get('/connexion', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->index();
    });

    $app->get('/deconnexion', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->deconnexion();
    })->name('deconnexion');

    $app->get('/catalogue', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->catalogue();
    })->name('catalogue');

    $app->post('/catalogue', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->ajoutPanier();
    });

    $app->get('/billet', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->billet();
    })->name('billet');

    $app->post('/billet', function () {
        $contrUser = new \limaga\control\UserController();
        if(isset($_POST['ajoutBillet']) && $_POST['ajoutBillet'] == 'ajBillet') {
            $contrUser->ajoutPanierBillet();
        }
        elseif(isset($_POST['ajoutAbonnement']) && $_POST['ajoutAbonnement'] == 'ajAbonnement') {
            $contrUser->ajoutPanierAbonnement();
        }
        elseif(isset($_POST['ajoutBilletFamille']) && $_POST['ajoutProduit'] == 'ajBilletFamille') {
            $contrUser->achatBilletFamille();
        }
    });

    $app->get('/panier', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->panier();
    })->name('panier');

    $app->get('/profil', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->profil();
    })->name('profil');

    $app->get('/modificationProfil', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->redirection();
    })->name('modificationProfil');

    $app->post('/modificationProfil', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->modifProfil();
    });

    $app->get('/ajoutMembreFamille', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->formMembreFamille();
    })->name('ajoutMembreFamille');

    $app->post('/ajoutMembreFamille', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->ajoutMembreFamille();
    });

    $app->post("/panier", function () {
    	$contrUser = new \limaga\control\UserController();
    	$contrUser->creerFacture();
    });

    $app->get('/horaire', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->horaire();
    })->name('horaire');

    $app->get('/plan', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->plan();
    })->name('plan');

    $app->get('/contact', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->contact();
    })->name('contact');

    $app->get('/actualite', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->actualite();
    })->name('actualite');

    $app->get('/prestation', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->prestation();
    })->name('prestation');

    $app->get('/facture', function() {
        $contrUser = new \limaga\control\UserController();
        $contrUser->facture();
    })->name('facture');

    $app->post('/facture', function () {
        $contrUser = new \limaga\control\UserController();
        $contrUser->factureReglement();
    });

    $app->post('/panier', function () {
       $contrUser = new \limaga\control\UserController();
        $contrUser->creerFacture();
    });
}

$app->run();


