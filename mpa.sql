-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Mer 20 Janvier 2016 à 09:46
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `pereirad9u`
--
CREATE DATABASE IF NOT EXISTS `mpa` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `mpa`;

-- --------------------------------------------------------

--
-- Structure de la table `abonnement`
--

CREATE TABLE IF NOT EXISTS `abonnement` (
  `idAbonnement` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(30) NOT NULL,
  `prix` double(5,2) NOT NULL,
  `codeBarre` int(11) NOT NULL,
  PRIMARY KEY (`idAbonnement`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `abonnement`
--

INSERT INTO `abonnement` (`idAbonnement`, `libelle`, `prix`, `codeBarre`) VALUES
(5, 'mensuel', 12.00, 328801269);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
  `idClient` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `dateNaiss` date NOT NULL,
  `ville` varchar(40) NOT NULL,
  `adresse` varchar(60) NOT NULL,
  `codePostal` int(5) NOT NULL,
  `telephone` int(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `niveauNatation` int(11) NOT NULL,
  `motDePasse` varchar(300) NOT NULL,
  PRIMARY KEY (`idClient`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `email_2` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`idClient`, `nom`, `prenom`, `dateNaiss`, `ville`, `adresse`, `codePostal`, `telephone`, `email`, `niveauNatation`, `motDePasse`) VALUES
(1, 'test', 'test', '2000-06-12', 'test', 'test', 11111, 111111111, 'test@test.fr', 1, '123');

-- --------------------------------------------------------

--
-- Structure de la table `contientabonnement`
--

CREATE TABLE IF NOT EXISTS `contientabonnement` (
  `id_panier` int(11) NOT NULL AUTO_INCREMENT,
  `id_abonnement` int(11) NOT NULL,
  PRIMARY KEY (`id_panier`,`id_abonnement`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=65 ;

-- --------------------------------------------------------

--
-- Structure de la table `contientbillet`
--

CREATE TABLE IF NOT EXISTS `contientbillet` (
  `id_panier` int(11) NOT NULL,
  `id_billet` int(11) NOT NULL,
  PRIMARY KEY (`id_panier`,`id_billet`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contientbilletfamille`
--

CREATE TABLE IF NOT EXISTS `contientbilletfamille` (
  `id_panier` int(11) NOT NULL,
  `id_billetFamille` int(11) NOT NULL,
  PRIMARY KEY (`id_panier`,`id_billetFamille`),
  KEY `id_billetFamille` (`id_billetFamille`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `contientproduit`
--

CREATE TABLE IF NOT EXISTS `contientproduit` (
  `id_panier` int(11) NOT NULL,
  `id_produit` int(11) NOT NULL,
  `quantite` int(11) NOT NULL,
  PRIMARY KEY (`id_panier`,`id_produit`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contientproduit`
--

INSERT INTO `contientproduit` (`id_panier`, `id_produit`, `quantite`) VALUES
(65, 1, 3),
(65, 2, 2);

-- --------------------------------------------------------

--
-- Structure de la table `ebillet`
--

CREATE TABLE IF NOT EXISTS `ebillet` (
  `idBillet` int(11) NOT NULL AUTO_INCREMENT,
  `codeBarre` varchar(20) NOT NULL,
  PRIMARY KEY (`idBillet`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Contenu de la table `ebillet`
--

INSERT INTO `ebillet` (`idBillet`, `codeBarre`) VALUES
(16, '319767761'),
(17, '410314331');

-- --------------------------------------------------------

--
-- Structure de la table `ebilletfamille`
--

CREATE TABLE IF NOT EXISTS `ebilletfamille` (
  `idBilletFamille` int(11) NOT NULL AUTO_INCREMENT,
  `codeBarre` int(9) NOT NULL,
  PRIMARY KEY (`idBilletFamille`),
  UNIQUE KEY `codeBarre` (`codeBarre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `facture`
--

CREATE TABLE IF NOT EXISTS `facture` (
  `idFacture` int(11) NOT NULL AUTO_INCREMENT,
  `montant` double(8,2) NOT NULL,
  `reglee` tinyint(1) NOT NULL,
  `moyenPaiment` int(11) NOT NULL,
  `dateCreation` date NOT NULL,
  `dateReglement` date NOT NULL,
  `id_client` int(11) NOT NULL,
  `id_panier` int(11) NOT NULL,
  PRIMARY KEY (`idFacture`),
  KEY `fk_facture1` (`id_client`),
  KEY `fk_facture2` (`id_panier`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

CREATE TABLE IF NOT EXISTS `famille` (
  `idClient` int(11) NOT NULL,
  `idMembre` int(11) NOT NULL,
  PRIMARY KEY (`idClient`,`idMembre`),
  KEY `idMembre` (`idMembre`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `membrefamille`
--

CREATE TABLE IF NOT EXISTS `membrefamille` (
  `idMembre` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `age` int(11) NOT NULL,
  PRIMARY KEY (`idMembre`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `membrefamille`
--

INSERT INTO `membrefamille` (`idMembre`, `nom`, `prenom`, `age`) VALUES
(1, 'tho', 'mas', 12),
(2, 'val', 'len', 12),
(3, 'vll', 'lll', 3),
(4, 'alll', 'lex', 2);

-- --------------------------------------------------------

--
-- Structure de la table `panier`
--

CREATE TABLE IF NOT EXISTS `panier` (
  `idPanier` int(11) NOT NULL AUTO_INCREMENT,
  `valide` tinyint(1) NOT NULL,
  `prixTotal` double(8,2) NOT NULL,
  `id_client` int(11) NOT NULL,
  PRIMARY KEY (`idPanier`),
  KEY `fk_panier` (`id_client`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=66 ;

--
-- Contenu de la table `panier`
--

INSERT INTO `panier` (`idPanier`, `valide`, `prixTotal`, `id_client`) VALUES
(65, 0, 160.00, 1);

-- --------------------------------------------------------

--
-- Structure de la table `produit`
--

CREATE TABLE IF NOT EXISTS `produit` (
  `idProduit` int(11) NOT NULL AUTO_INCREMENT,
  `libelle` varchar(40) NOT NULL,
  `description` text NOT NULL,
  `prix` double(5,2) NOT NULL,
  PRIMARY KEY (`idProduit`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `produit`
--

INSERT INTO `produit` (`idProduit`, `libelle`, `description`, `prix`) VALUES
(1, 'lunette', 'lunette pour la natation', 20.00),
(2, 'palmes', 'palmes de natation', 50.00);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `contientabonnement`
--
ALTER TABLE `contientabonnement`
  ADD CONSTRAINT `fk_contientabonnement` FOREIGN KEY (`id_panier`) REFERENCES `panier` (`idPanier`);

--
-- Contraintes pour la table `contientbillet`
--
ALTER TABLE `contientbillet`
  ADD CONSTRAINT `fk_contientbillet` FOREIGN KEY (`id_panier`) REFERENCES `panier` (`idPanier`);

--
-- Contraintes pour la table `contientbilletfamille`
--
ALTER TABLE `contientbilletfamille`
  ADD CONSTRAINT `contientbilletfamille_ibfk_1` FOREIGN KEY (`id_panier`) REFERENCES `client` (`idClient`),
  ADD CONSTRAINT `contientbilletfamille_ibfk_2` FOREIGN KEY (`id_billetFamille`) REFERENCES `ebilletfamille` (`idBilletFamille`),
  ADD CONSTRAINT `contientbilletfamille_ibfk_3` FOREIGN KEY (`id_panier`) REFERENCES `panier` (`idPanier`),
  ADD CONSTRAINT `contientbilletfamille_ibfk_4` FOREIGN KEY (`id_billetFamille`) REFERENCES `ebilletfamille` (`idBilletFamille`);

--
-- Contraintes pour la table `contientproduit`
--
ALTER TABLE `contientproduit`
  ADD CONSTRAINT `fk_contientproduit` FOREIGN KEY (`id_panier`) REFERENCES `panier` (`idPanier`);

--
-- Contraintes pour la table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `fk_facture1` FOREIGN KEY (`id_client`) REFERENCES `client` (`idClient`),
  ADD CONSTRAINT `fk_facture2` FOREIGN KEY (`id_panier`) REFERENCES `panier` (`idPanier`);

--
-- Contraintes pour la table `famille`
--
ALTER TABLE `famille`
  ADD CONSTRAINT `famille_ibfk_2` FOREIGN KEY (`idMembre`) REFERENCES `membrefamille` (`idMembre`),
  ADD CONSTRAINT `famille_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`);

--
-- Contraintes pour la table `panier`
--
ALTER TABLE `panier`
  ADD CONSTRAINT `fk_panier` FOREIGN KEY (`id_client`) REFERENCES `client` (`idClient`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
