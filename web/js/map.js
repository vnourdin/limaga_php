/**
 * Created by Nicolas on 19/01/2016.
 */
function initialisation(){
    var centreCarte = new google.maps.LatLng(48.686559, 6.210665);
    var optionsCarte = {
        zoom: 15,
        center: centreCarte,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var maCarte = new google.maps.Map(document.getElementById("emplacementMap"), optionsCarte);
    var optionsMarqueur = {
        position: maCarte.getCenter(),
        map: maCarte
    };
    var marqueur = new google.maps.Marker(optionsMarqueur);
}
google.maps.event.addDomListener(window, 'load', initialisation);
